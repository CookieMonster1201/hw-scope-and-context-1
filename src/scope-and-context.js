class Fighter {
  #name;
  #damage;
  #hp;
  #strength;
  #agility;
  #wins;
  #losses;
  constructor({name, damage, hp, strength, agility}) {
  this.#name = name;
  this.#damage = damage;
  this.#hp = hp;
  this.#strength = strength;
  this.#agility = agility;
  this.#wins = 0;
  this.#losses = 0;
  }

  getName() {
    return this.#name;
  }  
  getDamage() {
    return this.#damage;
  }
  getStrength() {
    return this.#strength;
  }
  getAgility() {
    return this.#agility;
  }
  getHealth() {
    return this.#hp;
  }
  addWin() {
    this.#wins += 1;
  }
  getWins() {
    return this.#wins;
  }
  addLoss() {
    this.#losses += 1;
  }
  getLosses() {
    return this.#losses;
  }
  logCombatHistory() {
    console.log(`Name:${this.getName()},Wins:${this.getWins()},Losses:${this.getLosses()}`)
  }
  heal(hPoints) {
    this.#hp += hPoints;
  }
  dealDamage(hPoints) {
    this.#hp = this.#hp - hPoints >= 0 ? this.#hp - hPoints : 0;
  }

  attack(defender) {
    const isSuccessful = getRandom();

    if (this.#strength + this.#agility > isSuccessful) {
      console.log(`${this.#name} makes ${this.#damage} damage to ${defender.getName()}`);
      defender.dealDamage(this.#damage)
    } else {
      console.log(`${this.#name} attack missed`);
    }
  }
}

const battle = function (fighter1, fighter2) {
  let winner;
  let loser;

  if (isDead(fighter1)) {
    logIsDead(fighter1);
    return 0;
  }
  if (isDead(fighter2)) {
    logIsDead(fighter2);
    return 0;
  }

  while (fighter2.getHealth() > 0 && fighter2.getHealth() > 0) {
    fighter1.attack(fighter2);
      if (fighter2.getHealth() > 0) {
          fighter2.attack(fighter1);
      }
  }
    
  [...arguments].forEach((fighter) => {
    if (!isDead(fighter)) {
      winner = fighter;
      winner.addWin()
      console.log(`${winner.getName()} has won!`);
    } else {       
      fighter.addLoss()
      loser = fighter;
    }
  });

  if (fighter2.getHealth() === 0 && fighter1.getHealth() === 0) {
    return 0;
  }
    return loser;
}


function getRandom() {
  return Math.floor(Math.random() * 101)
}
const isDead = (fighter) => {
  return fighter.getHealth() === 0;
};
const logIsDead = (fighter) => {
  console.log(`${fighter.getName()} is dead`);
};

console.log(Fighter)
module.exports = { Fighter, battle};
